var AppDispatcher = require('../dispatcher/AppDispatcher');
var ComboConstants = require('../constants/ComboConstants');
var ComboAPI = require('../utils/ComboAPI');
var EventEmitter = require('events').EventEmitter;
var _ = require('underscore');

var _comboSelects = [];

function loadCombos(data) {
  _comboSelects = data;
}


var ComboStore = _.extend(EventEmitter.prototype, {
  getCombos: function () {
    return _comboSelects;
  },
  emitChange: function () {
    this.emit('change');
  },
  addChangeListener: function (callback) {
    this.on('change', callback);
  },
  removeChangeListener: function (callback) {
    this.removeListener('change', callback);
  }

});

ComboStore.dispatcherIndex = AppDispatcher.register(function (payload) {
  var action = payload.action;
  console.log(action.actionType);
  switch (action.actionType) {
    case ComboConstants.LOAD_COMBOS:
      loadCombos(action.data);
      break;
    default:
      return true;
  }

  ComboStore.emitChange();

  return true;
});

module.exports = ComboStore;

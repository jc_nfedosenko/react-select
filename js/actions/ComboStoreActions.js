var AppDispatcher = require('../dispatcher/AppDispatcher');
var ComboStoreConstants = require('../constants/ComboConstants');

var ComboStoreActions = {

  loadCombos: function (data) {
    AppDispatcher.handleAction({
      actionType: ComboStoreConstants.LOAD_COMBOS,
      data: data
    });
  }

};

module.exports = ComboStoreActions;

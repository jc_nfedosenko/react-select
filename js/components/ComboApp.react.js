/**
 * React component for app with comboboxes
 */
var React = require('react');
var ComboSelect = require('../components/ComboSelect.react');
var ComboStore = require('../stores/ComboStore');

// Method to retrieve state from Stores
function getAppState() {
  return {
    comboSelect1: ComboStore.getCombos()
  };
}

var ComboApp = React.createClass({
  // Get initial state from stores
  getInitialState: function () {
    return getAppState();
  },
  // Add change listeners to stores
  componentDidMount: function () {
    ComboStore.addChangeListener(this._onChange);
  },
  // Remove change listeners from stores
  componentWillUnmount: function () {
    ComboStore.removeChangeListener(this._onChange);
  },
  render: function () {
    return (
      <div>
        <ComboSelect opts={this.state.comboSelect1}></ComboSelect>
      </div>
    );
  },
  _onChange: function () {
    this.setState(getAppState());
  }
});

module.exports = ComboApp;

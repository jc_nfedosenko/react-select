/**
 * React component for SelectBox
 */
var React = require('react');

var ComboSelect = React.createClass({
  render: function () {
    var options = this.props.opts.map(function (l, index) {
      return <option key={index} value={l.value}>{l.value}</option>;
    });
    return (
      <select className="form-control">
        {options}
      </select>
    );
  }
});

module.exports = ComboSelect;

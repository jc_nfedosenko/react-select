var HeroActions = require('../actions/ComboStoreActions');
var request = require('superagent');
module.exports = {

  // Load mock data from fake API
  getData: function () {
    request.get('http://www.mocky.io/v2/565c767410000019388a3608')
      .set('Accept', 'application/json')
      .end(function (err, response) {
        HeroActions.loadCombos(response.body);
      });
  }

};

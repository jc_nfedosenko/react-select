window.React = require('react');
var ComboAPI = require('./utils/ComboAPI');
var ComboApp = require('./components/ComboApp.react.js');

// Load Mock API Call
ComboAPI.getData();

// Render App Controller View
React.render(
  <ComboApp />,
  document.getElementById('main')
);
